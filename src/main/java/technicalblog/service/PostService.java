package technicalblog.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import technicalblog.Model.Post;

import technicalblog.repository.Postrepository;

import java.util.*;
import java.util.Date;

@Service
public class PostService {

    @Autowired
    Postrepository postrepository;

    public List<Post> getAllPosts(){
        return postrepository.getAllPost();
    }

    public void createPost(Post newPost){

        postrepository.createPost(newPost);

        System.out.println("Posted created at" + newPost);
    }

      public Post getPost(int id)
     {
          return   postrepository.getOnePost(id);
     }

     public void updatePost(Post updatedPost)
     {
         updatedPost.setDate(new Date());
         postrepository.updatePost(updatedPost);
     }


    public void deletePost(Integer postId)
    {

        postrepository.deletePost(postId);
    }





}
