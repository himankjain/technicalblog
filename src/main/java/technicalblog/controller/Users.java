package technicalblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import technicalblog.Model.User;
import technicalblog.service.PostService;
import technicalblog.service.UserService;


@Controller
public class Users {

    @Autowired
    private UserService userService;

    @Autowired
    private PostService postService;

    @RequestMapping("users/login")
    public String login(){
        return "users/login";
    }

    @RequestMapping("users/registration")
    public String resgistration(){
        return "users/registration";
    }

    @RequestMapping(value = "users/login", method = RequestMethod.POST)
    public String loginUser(User user){

        if(userService.loginUser(user))
            return "redirect:/posts";
        else
            return "users/login";
    }

    @RequestMapping(value = "users/registration", method = RequestMethod.POST)
    public String registerUser(User user){

        return "users/login";
       // User newUser = new User(user.getUsername(), user.getPassword());


    }


    @RequestMapping(value = "users/logout", method = RequestMethod.POST)
    public String logoutUser(Model model){

        model.addAttribute("posts",postService.getAllPosts());

        return "index";
    }
}
