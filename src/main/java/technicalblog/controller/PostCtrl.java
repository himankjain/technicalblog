package technicalblog.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import technicalblog.Model.Post;
import technicalblog.service.PostService;

import java.util.Date;


@Controller
public class PostCtrl {
    @Autowired
    private PostService postService;

    @RequestMapping("posts")
    public String getUserPosts(Model model){

        model.addAttribute("posts",postService.getAllPosts());

        return "posts";
    }

    @RequestMapping("posts/create")
    public String createPost(){

        return "users/createPost";
    }

    @RequestMapping(value = "posts/create", method = RequestMethod.POST )
    public String PostCreate(Post newPost){
        newPost.setDate(new Date());
        postService.createPost(newPost);
        return "redirect:/posts";

    }

    @RequestMapping(value = "/editPost", method = RequestMethod.GET)
    public String editPost(@RequestParam(name="postId") Integer postId, Model model) {
        Post post = postService.getPost(postId);
        model.addAttribute("post",post);
        return "posts/edit";
    }

    @RequestMapping(value = "/editPost", method = RequestMethod.PUT)
    public String editPostSubmit(@RequestParam(name="postId") Integer postId, Post updatedPost) {
        updatedPost.setId(postId);
        postService.updatePost(updatedPost);
        return "redirect:/posts";
    }

    @RequestMapping(value= "/deletePost", method = RequestMethod.DELETE)
    public String deletePost(@RequestParam(name = "postId") Integer postId)
    {

        postService.deletePost(postId);
        return "redirect:/posts";
    }


}
