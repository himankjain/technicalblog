package technicalblog.repository;

import org.springframework.stereotype.Repository;
import technicalblog.Model.Post;

import javax.persistence.*;
import java.util.List;

@Repository
public class Postrepository {

    @PersistenceUnit( name= "techblog" )
    private EntityManagerFactory emf;

    public List<Post> getAllPost(){

        EntityManager em = emf.createEntityManager();
        TypedQuery<Post> query = em.createQuery("SELECT q from Post q", Post.class);

        List<Post> resultList = query.getResultList();

        return resultList;

    }

    public Post getOnePost(int id){
        EntityManager em = emf.createEntityManager();
        return em.find(Post.class, id);
    }

    public Post createPost(Post newPost){
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            em.persist(newPost);
            transaction.commit();
        }catch(Exception e) {
            transaction.rollback();
        }

        return newPost;
    }

    public void updatePost(Post updatedPost){
        EntityManager em = emf.createEntityManager();
        EntityTransaction transaction = em.getTransaction();

        try {
            transaction.begin();
            em.merge(updatedPost);
            transaction.commit();
        }catch(Exception e) {
            transaction.rollback();
        }

       // return updatedPost;
    }

    public void deletePost(int deletePostid)
        {
            EntityManager em = emf.createEntityManager();
            EntityTransaction transaction = em.getTransaction();

            try {
                transaction.begin();
                Post deletePost = em.find(Post.class, deletePostid);
                em.remove(deletePost);
                transaction.commit();
            }catch(Exception e) {
                transaction.rollback();
            }


        }



}
